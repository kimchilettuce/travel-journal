import "./App.css";

import Footer from "./components/Footer";
import Body from "./components/Body";

function App() {
    return (
        <div className="App">
            <Footer />
            <Body />
        </div>
    );
}

export default App;
