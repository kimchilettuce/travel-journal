import React from "react";
import "./Card.css";

import pinLogo from "../assets/map-pin.svg";

export default function Card(props) {
    // props fields
    // {
    //     title;
    //     location;
    //     googleMapsUrl;
    //     startDate;
    //     endDate;
    //     description;
    //     imageUrl;
    // }

    return (
        <div className="card--container">
            <div className="card--image-container">
                <img
                    className="card--image"
                    src={props.imageUrl}
                    alt="Location Display"
                />
            </div>
            <div className="card--text-block">
                <MapDetails {...props} />
                <h1 className="card--text-block-title">{props.title}</h1>
                <h2 className="card--text-block-dates">
                    {props.startDate} - {props.endDate}
                </h2>
                <h3 className="card--text-block-description">
                    {props.description}
                </h3>
            </div>
        </div>
    );
}

function MapDetails(props) {
    return (
        <div className="card--map-details">
            <img
                className="card--map-details-pin"
                src={pinLogo}
                alt="Map Pin"
            />

            <h3 className="card--map-details-location">
                {props.location.toUpperCase()}
            </h3>
            <a
                className="card--map-details-link"
                target="_blank"
                rel="noreferrer"
                href={props.googleMapsUrl}
            >
                View on Google Maps
            </a>
        </div>
    );
}
