import React from "react";
import "./Footer.css";

import logo from "../assets/world-logo.svg";

export default function Footer() {
    return (
        <section className="footer--container">
            <img className="footer--logo" src={logo} alt="Logo" />
            <h1 className="footer--title">my travel journal.</h1>
        </section>
    );
}
