import React from "react";
import "./Body.css";

import Card from "./Card";
import data from "../data/data";

export default function Body() {
    const cardlist = data.map((elem) => {
        return (
            <>
                <Card key={elem.id} {...elem} />
                <hr key={1000 - elem.id} className="body--line-break" />
            </>
        );
    });

    return <section className="body--container">{cardlist}</section>;
}
